
_complete_denv() {
    list=$(denv list | tr '\n' ' ')
    _alternative "arguments:custom arg:($list)"
    return 0
}

compdef _complete_denv denv
